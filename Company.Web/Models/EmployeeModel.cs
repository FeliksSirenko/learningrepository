﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Company.DTO.Concrete;

namespace Company.Web.Models
{
     public class EmployeeModel
     {
          public int Id { get; set; }

          [StringLength(15, ErrorMessage = "The name is too long")]
          [Required(ErrorMessage = "Please, the name of the employee is obligatory for input")]
          public string FirstName { get; set; }

          [StringLength(15, ErrorMessage = "The last name is too long")]
          [Required(ErrorMessage = "Please, the name of the employee is obligatory for input")]
          public string LastName { get; set; }

          [Range(typeof(DateTime), "1/1/1900", "1/1/2018", ErrorMessage = "Date is out of Range")]
          [Required(ErrorMessage = "Please, input DoB")]
          public DateTime DoB { get; set; }

          [StringLength(25, ErrorMessage = "The name of the country is too long")]
          public string Citizenship { get; set; }

          [Range(0, 60, ErrorMessage = "Specified experience is not real, sorry :)")]
          public int Experience { get; set; }

          [Required(ErrorMessage = "Email is reuired to send you spam")]
          [EmailAddress(ErrorMessage = "This is not an Email. Please, check the entered Email again")]
          public string Email { get; set; }

          [Required] public int DepartmentId { get; set; }
          public string DepartmentName { get; set; }
          public SortAndFilterDto Filters { get; set; }
     }
}