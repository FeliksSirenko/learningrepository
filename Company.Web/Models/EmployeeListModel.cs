﻿using Company.DAL.Abstract;
using Company.DTO.Concrete;

namespace Company.Web.Models
{
     public class EmployeeListModel
     {
          public IPagedList<EmployeeDto> PagedEmployees { get; set; }
          public SortAndFilterDto Filters { get; set; }
     }
}