﻿using Company.DAL.Abstract;
using Company.DTO.Concrete;

namespace Company.Web.Models
{
     public class DepartmentListModel
     {
          public IPagedList<DepartmentDto> PagedDepartments { get; set; }
          public SortAndFilterDto Filters { get; set; }
     }
}