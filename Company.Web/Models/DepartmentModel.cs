﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Company.DTO.Concrete;

namespace Company.Web.Models
{
     public class DepartmentModel
     {
          public int Id { get; set; }

          [Required(ErrorMessage = "Please, the name of the department is obligatory for input")]
          [StringLength(maximumLength: 50, ErrorMessage =
               "The name of the department is very long. Please introduce the abbreviation.")]
          public string Name { get; set; }

          [Range(1, 1000, ErrorMessage = "Бюджет не должен быть меньше 1 и больше 1000")]
          public int Budget { get; set; }

          public string Description { get; set; }
          public IEnumerable<EmployeeDto> Employees { get; set; }
          public SortAndFilterDto Filters { get; set; }
     }
}