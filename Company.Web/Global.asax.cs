﻿using System.Data.Entity;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Company.BLL.Abstract;
using Company.BLL.Concrete;
using Company.DAL.Abstract;
using Company.DAL.Concrete;
using Company.DAL.Entities;
using Company.Web.Binders;
using Company.Web.Helpers;
using Company.Web.Models;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace Company.Web
{
     public class MvcApplication : System.Web.HttpApplication
     {
          protected void Application_Start()
          {
               AreaRegistration.RegisterAllAreas();
               FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
               RouteConfig.RegisterRoutes(RouteTable.Routes);
               BundleConfig.RegisterBundles(BundleTable.Bundles);
               ModelBinders.Binders.Add(typeof(DepartmentModel), new ManageDepratmentBinder());
               ModelBinders.Binders.Add(typeof(EmployeeModel), new ManageEmployeeBinder());
               ModelBinders.Binders.Add(typeof(DepartmentListModel), new ListBinder());
               ModelBinders.Binders.Add(typeof(EmployeeListModel), new ListBinder());
               var container = new Container();
               container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

               //Assembly[] assemblies = new[] { typeof(Service<>).Assembly };

               // Register your types, for instance:
               container.Register<DbContext, CompanyContext>(Lifestyle.Scoped);
               //container.Register<ICompanyContext, CompanyContext>(Lifestyle.Scoped);
               container.Register<IDepartmentRepository, DepartmentRepository>(Lifestyle.Scoped);
               container.Register<IEmployeeRepository, EmployeeRepository>(Lifestyle.Scoped);
               container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
               container.Register<IDepartmentService, DepartmentService>(Lifestyle.Scoped);
               container.Register<IEmployeeService, EmployeeService>(Lifestyle.Scoped);

               // This is an extension method from the integration package.
               container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
               container.Verify();
               DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
               Mapper.Initialize(c => c.AddProfile<MappingProfile>());
          }
     }
}
