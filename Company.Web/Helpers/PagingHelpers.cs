﻿using System;
using System.Text;
using System.Web.Mvc;

namespace Company.Web.Helpers
{
     public static class PagingHelpers
     {
          public static MvcHtmlString PageLinks(this HtmlHelper html, int TotalPages, int PageNumber,
               Func<int, string> pageUrl)
          {
               StringBuilder result = new StringBuilder();
               for (int i = 1; i <= TotalPages; i++)
               {
                    TagBuilder tag = new TagBuilder("a");
                    tag.MergeAttribute("href", pageUrl(i));
                    tag.InnerHtml = i.ToString();
                    if (i == PageNumber)
                    {
                         tag.AddCssClass("selected");
                         tag.AddCssClass("btn-primary");
                    }

                    tag.AddCssClass("btn");
                    result.Append(tag);
               }

               return MvcHtmlString.Create(result.ToString());
          }
     }
}