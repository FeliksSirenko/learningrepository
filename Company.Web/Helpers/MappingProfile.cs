﻿using AutoMapper;
using Company.DAL.Entities;
using Company.DTO.Concrete;
using Company.Web.Models;

namespace Company.Web.Helpers
{
     public class MappingProfile : Profile
     {
          public MappingProfile()
          {
               CreateMap<DepartmentDto, Department>().ForMember(dto => dto.Id, ent => ent.MapFrom(src => src.Id))
                    .ForMember(dto => dto.Name, ent => ent.MapFrom(src => src.Name))
                    .ForMember(dto => dto.Budget, ent => ent.MapFrom(src => src.Budget))
                    .ForMember(dto => dto.Description, ent => ent.MapFrom(src => src.Description))
                    .ForMember(dto => dto.Employees, ent => ent.MapFrom(src => src.Employees)).ReverseMap();

               CreateMap<DepartmentDto, DepartmentModel>().ForMember(dto => dto.Id, ent => ent.MapFrom(src => src.Id))
                    .ForMember(dto => dto.Name, ent => ent.MapFrom(src => src.Name))
                    .ForMember(dto => dto.Budget, ent => ent.MapFrom(src => src.Budget))
                    .ForMember(dto => dto.Description, ent => ent.MapFrom(src => src.Description))
                    .ForMember(dto => dto.Employees, ent => ent.MapFrom(src => src.Employees)).ReverseMap();

               CreateMap<EmployeeDto, Employee>().ForMember(dto => dto.Id, ent => ent.MapFrom(src => src.Id))
                    .ForMember(dto => dto.FirstName, ent => ent.MapFrom(src => src.FirstName))
                    .ForMember(dto => dto.LastName, ent => ent.MapFrom(src => src.LastName))
                    .ForMember(dto => dto.DoB, ent => ent.MapFrom(src => src.DoB))
                    .ForMember(dto => dto.Citizenship, ent => ent.MapFrom(src => src.Citizenship))
                    .ForMember(dto => dto.Email, ent => ent.MapFrom(src => src.Email))
                    .ForMember(dto => dto.Experience, ent => ent.MapFrom(src => src.Experience))
                    .ForMember(dto => dto.DepartmentId, ent => ent.MapFrom(src => src.DepartmentId)).ReverseMap();

               CreateMap<EmployeeDto, EmployeeModel>().ForMember(dto => dto.Id, ent => ent.MapFrom(src => src.Id))
                    .ForMember(dto => dto.FirstName, ent => ent.MapFrom(src => src.FirstName))
                    .ForMember(dto => dto.LastName, ent => ent.MapFrom(src => src.LastName))
                    .ForMember(dto => dto.DoB, ent => ent.MapFrom(src => src.DoB))
                    .ForMember(dto => dto.Citizenship, ent => ent.MapFrom(src => src.Citizenship))
                    .ForMember(dto => dto.Email, ent => ent.MapFrom(src => src.Email))
                    .ForMember(dto => dto.Experience, ent => ent.MapFrom(src => src.Experience))
                    .ForMember(dto => dto.DepartmentName, ent => ent.MapFrom(src => src.DepartmentName))
                    .ForMember(dto => dto.DepartmentId, ent => ent.MapFrom(src => src.DepartmentId)).ReverseMap();
          }
     }
}