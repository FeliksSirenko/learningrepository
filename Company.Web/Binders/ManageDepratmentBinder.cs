﻿using System.Web.Mvc;
using Company.DTO.Concrete;
using Company.Web.Models;

namespace Company.Web.Binders
{
     public class ManageDepratmentBinder : IModelBinder
     {
          public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
          {
               var valueProvider = bindingContext.ValueProvider;

               // Department
               string name = (string) valueProvider.GetValue("Name").ConvertTo(typeof(string));
               string description = (string) valueProvider.GetValue("Description").ConvertTo(typeof(string));
               int budget = (int) valueProvider.GetValue("Budget").ConvertTo(typeof(int));
               int id = (int) valueProvider.GetValue("Id").ConvertTo(typeof(int));

               // Filter
               int pageNumber = (int) valueProvider.GetValue("PageNumber").ConvertTo(typeof(int));
               int pageSize = (int) valueProvider.GetValue("PageSize").ConvertTo(typeof(int));
               string sortOrder = (string) valueProvider.GetValue("SortOrder").ConvertTo(typeof(string));
               string sortBy = (string) valueProvider.GetValue("SortBy").ConvertTo(typeof(string));
               string stringToFind = (string) valueProvider.GetValue("StringToFind").ConvertTo(typeof(string));

               SortAndFilterDto filter = new SortAndFilterDto
               {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortOrder = sortOrder,
                    SortBy = sortBy,
                    StringToFind = stringToFind
               };

               DepartmentModel departments = new DepartmentModel
               {
                    Id = id,
                    Name = name,
                    Description = description,
                    Budget = budget,
                    Filters = filter
               };

               return departments;
          }
     }
}