﻿using System.Web.Mvc;
using Company.DTO.Concrete;

namespace Company.Web.Binders
{
     public class ListBinder : IModelBinder
     {
          public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
          {
               var valueProvider = bindingContext.ValueProvider;
               int pageNumber = (int) valueProvider.GetValue("PageNumber").ConvertTo(typeof(int));
               int pageSize = (int) valueProvider.GetValue("PageSize").ConvertTo(typeof(int));
               string sortOrder = (string) valueProvider.GetValue("SortOrder").ConvertTo(typeof(string));
               string sortBy = (string) valueProvider.GetValue("SortBy").ConvertTo(typeof(string));
               string category = (string)valueProvider.GetValue("Category").ConvertTo(typeof(string));
               string stringToFind = (string) valueProvider.GetValue("StringToFind").ConvertTo(typeof(string));

               SortAndFilterDto filter = new SortAndFilterDto
               {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortOrder = sortOrder,
                    SortBy = sortBy,
                    Category = category,
                    StringToFind = stringToFind
               };

               return filter;
          }
     }
}