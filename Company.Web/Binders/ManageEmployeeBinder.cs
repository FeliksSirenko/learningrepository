﻿using System;
using System.Web.Mvc;
using Company.DTO.Concrete;
using Company.Web.Models;

namespace Company.Web.Binders
{
     public class ManageEmployeeBinder : IModelBinder
     {
          public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
          {
               var valueProvider = bindingContext.ValueProvider;

               // Employee
               int id = (int) valueProvider.GetValue("Id").ConvertTo(typeof(int));
               string firstName = (string) valueProvider.GetValue("FirstName").ConvertTo(typeof(string));
               string lastName = (string) valueProvider.GetValue("LastName").ConvertTo(typeof(string));
               DateTime doB = (DateTime) valueProvider.GetValue("DoB").ConvertTo(typeof(DateTime));
               int experience = (int) valueProvider.GetValue("Experience").ConvertTo(typeof(int));
               string citizenship = (string) valueProvider.GetValue("Citizenship").ConvertTo(typeof(string));
               string email = (string) valueProvider.GetValue("Email").ConvertTo(typeof(string));
               int departmentId = (int) valueProvider.GetValue("DepartmentId").ConvertTo(typeof(int));

               // Filter
               int pageNumber = (int) valueProvider.GetValue("PageNumber").ConvertTo(typeof(int));
               int pageSize = (int) valueProvider.GetValue("PageSize").ConvertTo(typeof(int));
               string sortOrder = (string) valueProvider.GetValue("SortOrder").ConvertTo(typeof(string));
               string sortBy = (string) valueProvider.GetValue("SortBy").ConvertTo(typeof(string));
               string stringToFind = (string) valueProvider.GetValue("StringToFind").ConvertTo(typeof(string));

               SortAndFilterDto filter = new SortAndFilterDto
               {
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortOrder = sortOrder,
                    SortBy = sortBy,
                    StringToFind = stringToFind
               };

               EmployeeModel employeeModel = new EmployeeModel
               {
                    Id = id,
                    FirstName = firstName,
                    LastName = lastName,
                    DoB = doB,
                    Experience = experience,
                    Citizenship = citizenship,
                    Email = email,
                    DepartmentId = departmentId,
               };

               employeeModel.Filters = filter;

               return employeeModel;
          }
     }
}