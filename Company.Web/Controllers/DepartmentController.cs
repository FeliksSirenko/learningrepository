﻿using System.Web.Mvc;
using System.Data;
using AutoMapper;
using Company.BLL.Abstract;
using Company.DTO.Concrete;
using Company.Web.Models;
using Company.Web.Binders;

namespace Company.Web.Controllers
{
     public class DepartmentController : Controller
     {
          private readonly IDepartmentService _service;

          public DepartmentController(IDepartmentService service)
          {
               _service = service;
          }

          [HttpGet]
          public ActionResult Index(
               int pageNumber = 1, 
               int pageSize = 5, 
               string sortOrder = "ascending",
               string sortBy = "Name", 
               string stringToFind = "", 
               string category = "default")
          {
               var filter = new SortAndFilterDto
               {
                    SortOrder = sortOrder,
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortBy = sortBy,
                    Category = category,
                    StringToFind = stringToFind
               };

               var pagedDepartmentsDto = _service.GetAll(filter);

               filter.TotalItems = pagedDepartmentsDto.TotalItems;

               return View(new DepartmentListModel {PagedDepartments = pagedDepartmentsDto, Filters = filter});
          }

          [HttpPost]
          public ActionResult Index(SortAndFilterDto filter)
          {
               var pagedDepartmentsDto = _service.GetAll(filter);

               filter.TotalItems = pagedDepartmentsDto.TotalItems;

               return View(new DepartmentListModel {PagedDepartments = pagedDepartmentsDto, Filters = filter});
          }

          [HttpGet]
          public ActionResult Manage(int id, SortAndFilterDto filter)
          {
               var managedDepartment = (id == 0) ? new DepartmentDto() : _service.FindBy(id);

               // While Mapping Employees becomes equal to "null"
               var departmentModel = Mapper.Map<DepartmentDto, DepartmentModel>(managedDepartment);

               departmentModel.Filters = filter;

               return managedDepartment != null
                    ? View("Modify", departmentModel)
                    : (ActionResult) View("Error", ViewBag.ErrorMessage = "Cannot find the Specified Department");
          }

          [HttpPost]
          public ActionResult Manage([ModelBinder(typeof(ManageDepratmentBinder))] DepartmentModel pagedDepartment)
          {
               ModelState.Clear();
               TryValidateModel(pagedDepartment);
               if (ModelState.IsValid)
               {
                    try
                    {
                         if (_service.IsUniqueField(pagedDepartment.Name, pagedDepartment.Id))
                         {
                              var departmentDto = Mapper.Map<DepartmentModel, DepartmentDto>(pagedDepartment);

                              _service.Manage(departmentDto);

                              return RedirectToAction("Index",
                                   new
                                   {
                                        pageNumber = 1,
                                        pageSize = pagedDepartment.Filters.PageSize,
                                        sortOrder = pagedDepartment.Filters.SortOrder,
                                        sortBy = pagedDepartment.Filters.SortBy,
                                        stringToFind = pagedDepartment.Filters.StringToFind,
                                        category = pagedDepartment.Filters.Category
                                   });
                         }
                         
                         ModelState.AddModelError("Name", @"The name you are trying to enter is already taken!");

                         return View("Modify", pagedDepartment);
                         
                    }
                    catch (DataException)
                    {
                         ModelState.AddModelError("", @"Some problems with Database. Contact your Admin!");

                         return RedirectToAction("Index",
                              new
                              {
                                   pageNumber = 1,
                                   pageSize = pagedDepartment.Filters.PageSize,
                                   sortOrder = pagedDepartment.Filters.SortOrder,
                                   sortBy = pagedDepartment.Filters.SortBy,
                                   stringToFind = pagedDepartment.Filters.StringToFind,
                                   category = pagedDepartment.Filters.Category
                              });
                    }
               }
               else
               {
                    return View("Modify", pagedDepartment);
               }
          }

          public ActionResult Delete(int id, SortAndFilterDto filters)
          {
               try
               {
                    _service.Delete(id);
                    return RedirectToAction("Index",
                         new
                         {
                              pageNumber = 1,
                              pageSize = filters.PageSize,
                              sortOrder = filters.SortOrder,
                              sortBy = filters.SortBy,
                              stringToFind = filters.StringToFind,
                              category = filters.Category

                         });
               }
               catch (DataException)
               {
                    return View("Error", ViewBag.ErrorMessage = "Some problems with Database. Contact your Admin!");
               }
          }
     }
}