﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Company.BLL.Abstract;
using Company.DTO.Concrete;
using Company.Web.Binders;
using Company.Web.Models;

namespace Company.Web.Controllers
{
     public class EmployeeController : Controller
     {
          private readonly IEmployeeService _service;
          private readonly IDepartmentService _serviceDepartment;

          public EmployeeController(IEmployeeService serviceEmployee, IDepartmentService departmentService)
          {
               _service = serviceEmployee;
               _serviceDepartment = departmentService;
          }

          [HttpGet]
          public ActionResult Index(int pageNumber = 1,
               int pageSize = 50,
               string sortOrder = "ascending",
               string sortBy = "LastName",
               string stringToFind = "",
               string category = "default")
          {
               var filter = new SortAndFilterDto
               {
                    SortOrder = sortOrder,
                    PageNumber = pageNumber,
                    PageSize = pageSize,
                    SortBy = sortBy,
                    StringToFind = stringToFind,
                    Category = category
               };

               var pagedEmployeeDto = _service.GetAll(filter);

               filter.TotalItems = pagedEmployeeDto.TotalItems;

               return View(new EmployeeListModel {PagedEmployees = pagedEmployeeDto, Filters = filter});
          }

          [HttpPost]
          public ActionResult Index(SortAndFilterDto filter)
          {
               var pagedEmployeesDto = _service.GetAll(filter);

               filter.TotalItems = pagedEmployeesDto.TotalItems;

               return View(new EmployeeListModel {PagedEmployees = pagedEmployeesDto, Filters = filter});
          }

          [HttpGet]
          public ActionResult Manage(int id, SortAndFilterDto filter)
          {
               var managedEmployee = (id == 0) ? new EmployeeDto() : _service.FindBy(id);

               var employeeModel = Mapper.Map<EmployeeDto, EmployeeModel>(managedEmployee);

               employeeModel.Filters = filter;

               if (managedEmployee != null)
               {
                    var departments = _serviceDepartment.GetAllDepartments()
                         .Select(d => new SelectListItem() {Value = d.Id.ToString(), Text = d.Name});
                    if (id != 0)
                    {
                         var selectedItem = departments.First(m => m.Value == managedEmployee.DepartmentId.ToString());
                         selectedItem.Selected = true;
                    }

                    ViewBag.Departments = departments;
                    return View("Modify", employeeModel);
               }
               else
               {
                    return View("Error", ViewBag.ErrorMessage = "Cannot find the Specified Employee");
               }
          }

          [HttpPost]
          public ActionResult Manage([ModelBinder(typeof(ManageEmployeeBinder))] EmployeeModel pagedEmployee)
          {
               ModelState.Clear();
               TryValidateModel(pagedEmployee);
               if (ModelState.IsValid)
               {
                    try
                    {
                         if (_service.IsUniqueField(pagedEmployee.Email, pagedEmployee.Id))
                         {
                              var employeeDto = Mapper.Map<EmployeeModel, EmployeeDto>(pagedEmployee);

                              _service.Manage(employeeDto);

                              return RedirectToAction("Index",
                                   new
                                   {
                                        pageNumber = 1,
                                        pageSize = pagedEmployee.Filters.PageSize,
                                        sortOrder = pagedEmployee.Filters.SortOrder,
                                        sortBy = pagedEmployee.Filters.SortBy,
                                        stringToFind = pagedEmployee.Filters.StringToFind,
                                        category = pagedEmployee.Filters.Category
                                   });
                         }
                         else
                         {
                              ModelState.AddModelError("", "Указанная почта уже занята! ");

                              var departments = _serviceDepartment.GetAllDepartments()
                                   .Select(d => new SelectListItem() {Value = d.Id.ToString(), Text = d.Name});
                              if (pagedEmployee.Id != 0)
                              {
                                   var selectedItem = departments.First(m =>
                                        m.Value == pagedEmployee.DepartmentId.ToString());
                                   selectedItem.Selected = true;
                              }

                              ViewBag.Departments = departments;
                              return View("Modify", pagedEmployee);
                         }
                    }
                    catch (DataException)
                    {
                         ModelState.AddModelError("", "Some problems with Database. Contact your Admin!");
                         return RedirectToAction("Index",
                              new
                              {
                                   pageNumber = 1,
                                   pageSize = pagedEmployee.Filters.PageSize,
                                   sortOrder = pagedEmployee.Filters.SortOrder,
                                   sortBy = pagedEmployee.Filters.SortBy,
                                   stringToFind = pagedEmployee.Filters.StringToFind,
                                   category = pagedEmployee.Filters.Category
                              });
                    }
               }
               else
               {
                    ModelState.AddModelError("", "Unable to save changes because of model validation failure! ");

                    var departments = _serviceDepartment.GetAllDepartments()
                         .Select(d => new SelectListItem() {Value = d.Id.ToString(), Text = d.Name});

                    var selectedItem = departments.First(m => m.Value == pagedEmployee.DepartmentId.ToString());
                    selectedItem.Selected = true;

                    ViewBag.Departments = departments;
                    return View("Modify", pagedEmployee);
               }
          }

          [HttpGet]
          public ActionResult Delete(int id, SortAndFilterDto filters)
          {
               try
               {
                    _service.Delete(id);

                    return RedirectToAction("Index",
                         new
                         {
                              pageNumber = 1,
                              pageSize = filters.PageSize,
                              sortOrder = filters.SortOrder,
                              sortBy = filters.SortBy,
                              stringToFind = filters.StringToFind,
                              category = filters.Category
                         });
               }
               catch
               {
                    return View("Error", ViewBag.ErrorMessage = "Some problems with Database. Contact your Admin!");
               }
          }
     }
}