﻿using Company.DAL.Abstract;
using Company.DTO.Concrete;

namespace Company.BLL.Abstract
{
     public interface IService<T1> where T1 : class
     {
          IPagedList<T1> GetAll(SortAndFilterDto querySettings);
          void Manage(T1 entity);
          void Delete(int id);
          T1 FindBy(int id);
          bool IsUniqueField(string fieldValue, int keyValue);
     }
}