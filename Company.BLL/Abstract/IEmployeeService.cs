﻿using Company.DTO.Concrete;

namespace Company.BLL.Abstract
{
     public interface IEmployeeService : IService<EmployeeDto>
     {
     }
}