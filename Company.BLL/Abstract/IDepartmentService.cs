﻿using System.Collections.Generic;
using Company.DTO.Concrete;

namespace Company.BLL.Abstract
{
     public interface IDepartmentService : IService<DepartmentDto>
     {
          List<DepartmentDto> GetAllDepartments();
     }
}