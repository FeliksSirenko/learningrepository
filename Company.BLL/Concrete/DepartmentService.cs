﻿using System.Collections.Generic;
using Company.BLL.Abstract;
using Company.DAL.Abstract;
using Company.DTO.Concrete;

namespace Company.BLL.Concrete
{
     public class DepartmentService : IDepartmentService
     {
          private readonly IUnitOfWork _unitOfWork;
          private readonly IDepartmentRepository _departmentRepository;

          public DepartmentService(IUnitOfWork unitOfWork, IDepartmentRepository departmentRepository)
          {
               _unitOfWork = unitOfWork;
               _departmentRepository = departmentRepository;
          }

          public IPagedList<DepartmentDto> GetAll(SortAndFilterDto filters)
          {
               IPagedList<DepartmentDto> departmnetDtoS = _departmentRepository.GetAll(filters);
               return departmnetDtoS;
          }

          public void Manage(DepartmentDto entityDepartmentDto)
          {
               _departmentRepository.Manage(entityDepartmentDto);
               _unitOfWork.Commit();
          }

          public DepartmentDto FindBy(int id)
          {
               DepartmentDto departmentDto = _departmentRepository.GetById(id);
               return departmentDto;
          }

          public void Delete(int id)
          {
               _departmentRepository.Delete(id);
               _unitOfWork.Commit();
          }

          public bool IsUniqueField(string fieldValue, int keyValue) =>
               _departmentRepository.CheckUnique(fieldValue, keyValue);

          public List<DepartmentDto> GetAllDepartments() => _departmentRepository.GetAllDepartments();
     }
}