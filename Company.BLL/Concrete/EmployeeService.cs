﻿using Company.BLL.Abstract;
using Company.DAL.Abstract;
using Company.DTO.Concrete;

namespace Company.BLL.Concrete
{
     public class EmployeeService : IEmployeeService
     {
          private readonly IUnitOfWork _unitOfWork;
          private readonly IEmployeeRepository _employeeRepository;
          private readonly IDepartmentRepository _departmentRepository;

          public EmployeeService(IUnitOfWork unitOfWork, IEmployeeRepository employeeRepository,
               IDepartmentRepository departmentRepository)
          {
               _unitOfWork = unitOfWork;
               _employeeRepository = employeeRepository;
               _departmentRepository = departmentRepository;
          }

          public IPagedList<EmployeeDto> GetAll(SortAndFilterDto querySettings)
          {
               IPagedList<EmployeeDto> pagedEmployees = _employeeRepository.GetAll(querySettings);
               return pagedEmployees;
          }

          public void Manage(EmployeeDto employeeDto)
          {
               var DepartmentDto = _departmentRepository.GetDepartment(employeeDto.DepartmentId);
               employeeDto.DepartmentDto = DepartmentDto;
               _employeeRepository.Manage(employeeDto);
               _unitOfWork.Commit();
          }

          public EmployeeDto FindBy(int id)
          {
               EmployeeDto employeeDto = _employeeRepository.GetById(id);
               return employeeDto;
          }

          public void Delete(int id)
          {
               _employeeRepository.Delete(id);
               _unitOfWork.Commit();
          }

          public bool IsUniqueField(string fieldValue, int keyValue) =>
               _employeeRepository.CheckUnique(fieldValue, keyValue);
     }
}