﻿using System;
using System.Linq;
using Company.DAL.Entities;

namespace Company.BaseFiller
{
     class Program
     {
          private static Random random = new Random();

          static void Main()
          {
               var context = new CompanyContext();

               // Fill the departments
               for (int i = 1; i <= 50; i++)
               {
                    context.Departments.Add(
                         new Department
                         {
                              Name = RandomString(10),
                              Budget = random.Next(2, 999),
                              Description = RandomString(15) + "i" + RandomString(15)

                         });
                    Console.WriteLine(i);
               }

               context.SaveChanges();

               // Fill the employees
               for (int i = 1; i <= 1000; i++)
               {
                    context.Employees.Add(
                         new Employee
                         {
                              FirstName = RandomString(10),
                              LastName = RandomString(10),
                              DoB = new DateTime(random.Next(1950, 2010), random.Next(1, 12), random.Next(1, 27)),
                              Citizenship = "Украина",
                              Experience = random.Next(1, 20),
                              Email = RandomString(6) + "@gmail.com",
                              DepartmentId = random.Next(1, 50)

                         });
                    Console.WriteLine(i);
               }
               context.SaveChanges();
          }

          private static string RandomString(int length)
          {
               const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
               return new string(Enumerable.Repeat(chars, length)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
          }
     }
}
