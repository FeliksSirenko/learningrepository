﻿using System;

namespace Company.DTO.Concrete
{
     public class SortAndFilterDto
     {
          // Paging
          public int PageNumber { get; set; }
          public int PageSize { get; set; }
          public int TotalItems { get; set; }

          public int TotalPages
          {
               get => (PageSize != 0) ? ((int) Math.Ceiling((decimal) TotalItems / PageSize)) : default(int);
          }

          // Sorting
          public string SortOrder { get; set; }
          public string SortBy { get; set; }

          // Search
          public string Category { get; set; }
          public string StringToFind { get; set; }
     }
}