﻿using System;
using Company.DTO.Abstract;

namespace Company.DTO.Concrete
{
     public class EmployeeDto : IDtoObject
     {
          public int Id { get; set; }
          public string FirstName { get; set; }
          public string LastName { get; set; }
          public DateTime DoB { get; set; }
          public string Citizenship { get; set; }
          public int Experience { get; set; }
          public string Email { get; set; }
          public int DepartmentId { get; set; }
          public string DepartmentName { get; set; }
          public DepartmentDto DepartmentDto { get; set; }
     }
}
