﻿using System.Collections.Generic;
using Company.DTO.Abstract;

namespace Company.DTO.Concrete
{
     public class DepartmentDto : IDtoObject
     {
          public int Id { get; set; }
          public string Name { get; set; }
          public int Budget { get; set; }
          public string Description { get; set; }
          public IEnumerable<EmployeeDto> Employees { get; set; }
     }
}