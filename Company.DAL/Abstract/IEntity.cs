﻿namespace Company.DAL.Abstract
{
     public interface IEntity
     {
          int Id { get; set; }
     }
}