﻿using System;

namespace Company.DAL.Abstract
{
     public interface IUnitOfWork : IDisposable
     {
          int Commit();
     }
}