﻿using System.Collections.Generic;

namespace Company.DAL.Abstract
{
     public interface IPagedList<T>
     {
          // Paging
          int PageNumber { get; set; }
          int PageSize { get; set; }
          int TotalItems { get; set; }
          IList<T> List { get; set; }
     }
}