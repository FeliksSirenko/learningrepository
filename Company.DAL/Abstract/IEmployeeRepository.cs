﻿using Company.DTO.Concrete;

namespace Company.DAL.Abstract
{
     public interface IEmployeeRepository : IBaseRepository<EmployeeDto>
     {
          IPagedList<EmployeeDto> GetAll(SortAndFilterDto settings);
          bool CheckUnique(string field, int id);
     }
}