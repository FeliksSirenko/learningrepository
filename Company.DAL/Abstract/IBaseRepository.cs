﻿namespace Company.DAL.Abstract
{
     public interface IBaseRepository<T>
     {
          void Manage(T dto);

          //bool CheckUnique(string entityFieldName, string entityFieldValue, string entityKeyName, int? entityKeyValue);
          T GetById(int id);
          void Delete(int id);
     }
}
