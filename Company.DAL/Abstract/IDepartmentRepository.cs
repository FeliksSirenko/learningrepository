﻿using System.Collections.Generic;
using Company.DTO.Concrete;

namespace Company.DAL.Abstract
{
     public interface IDepartmentRepository : IBaseRepository<DepartmentDto>
     {
          IPagedList<DepartmentDto> GetAll(SortAndFilterDto settings);
          DepartmentDto GetDepartment(int id);
          List<DepartmentDto> GetAllDepartments();
          bool CheckUnique(string field, int id);
     }
}