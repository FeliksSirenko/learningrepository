﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Company.DAL.Entities.Mapping
{
     class DepartmentMap : EntityTypeConfiguration<Department>
     {
          public DepartmentMap()
          {
               // Primary key
               this.HasKey<int>(k => k.Id);

               // Properties
               this.Property(k => k.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                    .IsRequired();

               this.Property(k => k.Name).HasMaxLength(128)
                    .IsRequired()
                    .HasColumnOrder(2);
          }
     }
}
