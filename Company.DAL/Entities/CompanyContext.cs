﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Company.DAL.Entities
{
     public class CompanyContext : DbContext
     {
          public CompanyContext() : base("CompanyDb")
          {
               Database.SetInitializer<CompanyContext>(new CompanyDbInitializer());
               this.Configuration.LazyLoadingEnabled = false;
          }

          protected override void OnModelCreating(DbModelBuilder modelBuilder)
          {
               // Names of Tables
               modelBuilder.Entity<Employee>().ToTable("EmployeeInfo");
               modelBuilder.Entity<Department>().ToTable("DepartmentInfo");

               // Primary keys
               modelBuilder.Entity<Employee>().HasKey<int>(e => e.Id);
               modelBuilder.Entity<Department>().HasKey<int>(d => d.Id);
               modelBuilder.Entity<Employee>().Property<int>(e => e.Id)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
               modelBuilder.Entity<Department>().Property<int>(e => e.Id)
                    .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

               // Cascade deleting option (false)
               modelBuilder.Entity<Department>().HasMany<Employee>(d => d.Employees).WithRequired(c => c.Department)
                    .WillCascadeOnDelete(true);
               base.OnModelCreating(modelBuilder);
          }

          public DbSet<Department> Departments { get; set; }
          public DbSet<Employee> Employees { get; set; }

          public class CompanyDbInitializer : DropCreateDatabaseIfModelChanges<DbContext>
          {
               protected override void Seed(DbContext context)
               {
                    Department InitialDepartment = new Department()
                    {
                         Name = "IT department",
                         Description = "This department deals with different things about the computers.",
                         Budget = 100,
                    };
                    (context as CompanyContext).Departments.Add(InitialDepartment);
                    context.SaveChanges();
                    IEnumerable<Employee> InitialEmployees = new List<Employee>()
                    {
                         new Employee
                         {
                              FirstName = "Feliks",
                              LastName = "Sirenko",
                              DoB = new DateTime(1986, 12, 4),
                              Citizenship = "Ukraine",
                              Experience = 10,
                              Email = "sirenkofelix@gmail.com",
                              DepartmentId = 1
                         },
                         new Employee
                         {
                              FirstName = "Roman",
                              LastName = "Kravchenko",
                              DoB = new DateTime(1976, 11, 12),
                              Citizenship = "Russia",
                              Experience = 13,
                              Email = "Roman@gmail.com",
                              DepartmentId = 1
                         }
                    };
                    (context as CompanyContext).Employees.AddRange(InitialEmployees);
                    context.SaveChanges();
                    base.Seed(context);
               }
          }
     }
}