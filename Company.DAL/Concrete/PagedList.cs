﻿using System.Collections.Generic;
using Company.DAL.Abstract;

namespace Company.DAL.Concrete
{
     public class PagedList<T> : IPagedList<T>
     {
          public int PageNumber { get; set; }
          public int PageSize { get; set; }
          public int TotalItems { get; set; }
          public IList<T> List { get; set; }
     }
}