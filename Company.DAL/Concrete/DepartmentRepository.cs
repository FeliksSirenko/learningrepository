﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Company.DAL.Abstract;
using Company.DAL.Entities;
using System.Linq.Dynamic;
using AutoMapper;
using Company.DAL.Helpers;
using Company.DTO.Concrete;

namespace Company.DAL.Concrete
{
     public class DepartmentRepository : BaseRepository<DepartmentDto, Department>, IDepartmentRepository
     {
          public DepartmentRepository(DbContext context) : base(context)
          {
          }

          public IPagedList<DepartmentDto> GetAll(SortAndFilterDto settings)
          {
               var query = _db.AsQueryable();
               var searchString = settings.StringToFind;
               var searchCategory = settings.Category;

               if (!string.IsNullOrEmpty(searchString))
               {
                    query = query.Where(QueryHelper.SearchDepartmentExpression(searchCategory, searchString));
               }

               query = query.OrderBy($"{settings.SortBy} {settings.SortOrder}")
                    .Page(settings.PageNumber, settings.PageSize);

               query.Include(x => x.Employees);

               var departments = query.ToList();

               var departmentDtoS = departments.Select(Mapper.Map<Department, DepartmentDto>).ToList();

               var pagedDepartmentDtos = new PagedList<DepartmentDto>
               {
                    PageNumber = settings.PageNumber,
                    PageSize = settings.PageSize,
                    TotalItems = GetAmountOfEntities(settings),
                    List = departmentDtoS
               };
               return pagedDepartmentDtos;
          }

          public List<DepartmentDto> GetAllDepartments()
          {
               var departments = _db.ToList();
               var departmentDtoS = departments.Select(Mapper.Map<Department, DepartmentDto>).ToList();
               return departmentDtoS;
          }

          public DepartmentDto GetDepartment(int id)
          {
               var department = _db.SingleOrDefault(x => x.Id == id);
               var departmentDto = Mapper.Map<Department, DepartmentDto>(department);
               return departmentDto;
          }

          public bool CheckUnique(string field, int id)
          {
               var result = (_db.FirstOrDefault(m => m.Name == field && m.Id != id) == null);
               return result;
          }

          private int GetAmountOfEntities(SortAndFilterDto settings)
          {
               var searchString = settings.StringToFind;
               var searchCategory = settings.Category;
               var amount = (searchString != null) ? _db.Count(QueryHelper.SearchDepartmentExpression(searchCategory, searchString)) : _db.Count();
               return amount;
          }
     }
}