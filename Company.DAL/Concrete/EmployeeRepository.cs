﻿using AutoMapper;
using Company.DAL.Abstract;
using Company.DAL.Entities;
using Company.DTO.Concrete;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Remoting.Messaging;
using Company.DAL.Helpers;

namespace Company.DAL.Concrete
{
     public class EmployeeRepository : BaseRepository<EmployeeDto, Employee>, IEmployeeRepository
     {
          public EmployeeRepository(DbContext context) : base(context)
          {
          }

          public IPagedList<EmployeeDto> GetAll(SortAndFilterDto settings)
          {
               var query = _db.AsQueryable();
               var searchString = settings.StringToFind;
               var searchCategory = settings.Category;


               if (!string.IsNullOrEmpty(searchString))
               {
                    query = query.Where(QueryHelper.SearchEmployeeExpression(searchCategory, searchString));
               }

               query = query.OrderBy($"{settings.SortBy} {settings.SortOrder}")
                            .Page(settings.PageNumber, settings.PageSize);

               query.Include(x => x.Department);

               var employees = query.ToList();


               var employeeDtoS = employees.Select(Mapper.Map<Employee, EmployeeDto>).ToList();

               var pagedEmployeeDtoS = new PagedList<EmployeeDto>
               {
                    PageNumber = settings.PageNumber,
                    PageSize = settings.PageSize,
                    TotalItems = GetAmountOfEntities(settings),
                    List = employeeDtoS
               };

               return pagedEmployeeDtoS;
          }

          public bool CheckUnique(string field, int id)
          {
               var result = (_db.FirstOrDefault(m => m.Email == field && m.Id != id) == null);
               return result;
          }

          private int GetAmountOfEntities(SortAndFilterDto settings)
          {
               var searchString = settings.StringToFind;
               var searchCategory = settings.Category;

               var amount = (searchString != null) ?
                    _db.Count(QueryHelper.SearchEmployeeExpression(searchCategory, searchString)) :
                    _db.Count();

               return amount;
          }
     }
}