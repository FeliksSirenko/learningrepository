﻿using System.Data.Entity;
using System.Linq;
using Company.DAL.Abstract;
using AutoMapper;
using Company.DTO.Abstract;
using Company.DTO.Concrete;

namespace Company.DAL.Concrete
{
     public class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
          where TDto : IDtoObject where TEntity : class, IEntity
     {
          protected DbContext _context;
          protected readonly DbSet<TEntity> _db;

          public BaseRepository(DbContext context)
          {
               _context = context;
               _db = context.Set<TEntity>();
          }

          public void Manage(TDto dto)
          {
               var entity = Mapper.Map<TDto, TEntity>(dto);

               if (entity.Id != 0)
               {
                    _db.Attach(entity);
                    _context.Entry(entity).State = EntityState.Modified;
               }
               else
               {
                    _db.Add(entity);
                    _context.Entry(entity).State = EntityState.Added;
               }
          }

          public TDto GetById(int id)
          {
               var entity = _db.Find(id);

               var entityDto = (TDto) Mapper.Map(entity, typeof(TEntity), typeof(TDto));

               return entityDto;
          }

          public void Delete(int id)
          {
               var entity = _db.Find(id);

               if (entity != null)
               {
                    _db.Remove(entity);
               }
          }
    }
}