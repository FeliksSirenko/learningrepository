﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Reflection;

namespace Company.DAL.Helpers
{
     public static class LinqHelper
     {
          public static IQueryable<T> FirstOrDefault<T>(this IQueryable<T> source, string fieldName, string fieldValue,
               string keyName, int? keyValue)
          {
               ParameterExpression parameterExpression = Expression.Parameter(typeof(T), "x");
               MemberExpression propertyOne = Expression.Property(parameterExpression, fieldName);
               MemberExpression propertyTwo = Expression.Property(parameterExpression, keyName);
               ConstantExpression constantOne = Expression.Constant(fieldValue);
               ConstantExpression constantTwo = Expression.Constant(keyValue);
               Expression firstExpression = Expression.Equal(propertyOne, constantOne);
               Expression secondExpression = Expression.NotEqual(propertyTwo, constantTwo);
               Expression finalExpression = Expression.And(firstExpression, secondExpression);
               LambdaExpression lambdaExpression =
                    Expression.Lambda<Func<T, bool>>(finalExpression, parameterExpression);
               Delegate lambda = lambdaExpression.Compile();
               Type queryableType = typeof(Queryable);
               MethodInfo[] methods = queryableType.GetMethods(BindingFlags.Public | BindingFlags.Static);
               var selectedMethod = methods.Where(m => m.Name == "FirstOrDefault" && m.GetParameters().Length == 2);
               MethodInfo method = selectedMethod.First();
               method = method.MakeGenericMethod(finalExpression.Type);
               var result = (IQueryable<T>) method.Invoke(null, new object[] {source, lambda});
               return result;
          }

          public static IQueryable<T> Page<T>(this IQueryable<T> source, int number, int size)
          {
               var query = source.Skip((number - 1) * size).Take(size);
               return query;
          }
     }
}