﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Company.DAL.Entities;

namespace Company.DAL.Helpers
{
     public static class QueryHelper
     {
          public static Expression<Func<Employee, bool>> SearchEmployeeExpression(string category, string searchWord)
          {
               Expression<Func<Employee, bool>> linqEmployee;


               if (category != "default")
               {
                    switch (category)
                    {
                         case "FirstName":
                              linqEmployee = x => x.FirstName.Contains(searchWord);
                              break;

                         case "LastName":
                              linqEmployee = x => x.LastName.Contains(searchWord);
                              break;

                         case "Citizenship":
                              linqEmployee = x => x.Citizenship.Contains(searchWord);
                              break;

                         case "Email":
                              linqEmployee = x => x.Email.Contains(searchWord);
                              break;

                         case "Name":
                              linqEmployee = x => x.Department.Name.Contains(searchWord);
                              break;

                         //case "DoB":
                         //     linqEmployee = x => x.DoB.ToShortDateString().Contains(searchWord);
                         //     break;

                         default: // "Experience"
                              linqEmployee = x => x.Experience.ToString().Contains(searchWord);
                              break;
                    }
               }
               else
               {
                    linqEmployee = x => x.FirstName.Contains(searchWord) ||
                                        x.LastName.Contains(searchWord) ||
                                        x.Citizenship.Contains(searchWord) ||
                                        x.Email.Contains(searchWord) ||
                                        x.Experience.ToString().Contains(searchWord) ||
                                        x.DoB.ToString().Contains(searchWord) ||
                                        x.Department.Name.Contains(searchWord);
               }

               return linqEmployee;
          }

          public static Expression<Func<Department, bool>> SearchDepartmentExpression(string category, string searchWord)
          {

               Expression<Func<Department, bool>> linqDepartment;

               if (category != "default")
               {
                    switch (category)
                    {
                         case "Description":
                              linqDepartment = x => x.Description.Contains(searchWord);
                              break;

                         case "Budget":
                              linqDepartment = x => x.Budget.ToString().Contains(searchWord);
                              break;

                         default: // "Name"
                              linqDepartment = x => x.Name.Contains(searchWord);
                              break;
                    }
               }
               else
               {
                    linqDepartment = x => x.Name.Contains(searchWord) ||
                                          x.Budget.ToString().Contains(searchWord) ||
                                          x.Description.Contains(searchWord);
               }

               return linqDepartment;
          }
     }
}